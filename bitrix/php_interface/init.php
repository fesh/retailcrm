<?php

AddEventHandler('form', 'onAfterResultAdd', 'callForm_onAfterResultAddUpdate');
AddEventHandler('form', 'onAfterResultUpdate', 'callForm_onAfterResultAddUpdate');
AddEventHandler('sale', 'OnSaleCalculateOrder', 'callForm_OnSaleCalculateOrder');

function callForm_OnSaleCalculateOrder(&$arFields) {
	if (!CModule::IncludeModule('sale')) {
		return;
	}
	$sity = CSaleLocation::GetByID($arFields['ORDER_PROP'][6]);
	$arFields['ORDER_PROP'][5] = $sity["CITY_NAME"];
	return true;
}

function callForm_onAfterResultAddUpdate($WEB_FORM_ID, $RESULT_ID) {
	global $USER;
	if (!CModule::IncludeModule('sale')) {
		return;
	}

	// действие обработчика распространяется только на форму с обратным звонком
	if ($WEB_FORM_ID == 1) {
		//Получаем результаты заполнения формы
		$arFormAnswer = CFormResult::GetDataByID(
			$RESULT_ID,
			array(),
			$arResult,
			$arFormAnswer2);
		//Предварительно регистрируем пользователя, если он не зарегистрирован
		$user = array();
		$userId = null;
		if (!$USER->IsAuthorized()) {

			//Пытаемся сначала авторизоваться с поступившими данными
			$rsUser = CUser::GetByLogin(htmlspecialcharsbx($arFormAnswer["PHONE"][0]["USER_TEXT"]));
			$arUser = $rsUser->Fetch();
			if (!empty($arUser)) {
				$userId = $arUser["ID"];
				$upUser = new CUser;
				$upUser->Update($userId, array("NAME" => htmlspecialcharsbx($arFormAnswer["NAME"][0]["USER_TEXT"])));
				$arUser["NAME"] = htmlspecialcharsbx($arFormAnswer["NAME"][0]["USER_TEXT"]);
				$user = $arUser;
			}
			//Если такого пользователя нет, то пытаемся его зарегистрировать
			else {
				$user["LOGIN"] = htmlspecialcharsbx($arFormAnswer["PHONE"][0]["USER_TEXT"]);
				$user["NAME"] = htmlspecialcharsbx($arFormAnswer["NAME"][0]["USER_TEXT"]);
				$user["PERSONAL_PHONE"] = htmlspecialcharsbx($arFormAnswer["PHONE"][0]["USER_TEXT"]);
				$user["EMAIL"] = htmlspecialcharsbx($arFormAnswer["PHONE"][0]["USER_TEXT"]) . "@temp.temp";

				$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
				if ($def_group != "") {
					$GROUP_ID = explode(",", $def_group);
					$arPolicy = $USER->GetGroupPolicy($GROUP_ID);
				} else {
					$arPolicy = $USER->GetGroupPolicy(array());
				}
				$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
				if ($password_min_length <= 0) {
					$password_min_length = 6;
				}

				$password_chars = array(
					"abcdefghijklnmopqrstuvwxyz",
					"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
					"0123456789",
				);
				if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y") {
					$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
				}

				$user["PASSWORD"] = $user["PASSWORD_CONFIRM"] = randString($password_min_length + 2, $password_chars);
				$newUser = new CUser;
				if (!($userId = $newUser->Add($user))) {
					echo $user->LAST_ERROR;
				}

			}
		} else {
			$rsUser = CUser::GetByID($USER->GetID());
			$user = $rsUser->Fetch();
		}
		//Создаём заказ
		if ($userId !== null) {
			$arOrderFields = array(
				"LID" => "s1",
				"PERSON_TYPE_ID" => 1,
				"PAYED" => "N",
				"CANCELED" => "N",
				"STATUS_ID" => "N",
				"PRICE" => 0,
				"CURRENCY" => "RUB",
				"USER_ID" => $userId,
				"PAY_SYSTEM_ID" => 3,
				"PRICE_DELIVERY" => 0,
				"DELIVERY_ID" => 2,
				"DISCOUNT_VALUE" => 0,
				"TAX_VALUE" => 0.0,
				"USER_DESCRIPTION" => "",
			);
			$ORDER_ID = CSaleOrder::Add($arOrderFields);
			//Если заказ был добавлен, то устанавливаем дополнительные свойства заказа
			if ($ORDER_ID) {
				AddOrderProperty("FIO", htmlspecialcharsbx($arFormAnswer["NAME"][0]["USER_TEXT"]), $ORDER_ID);
				AddOrderProperty("PHONE", htmlspecialcharsbx($arFormAnswer["PHONE"][0]["USER_TEXT"]), $ORDER_ID);
				AddOrderProperty("orderMethod", "back-call", $ORDER_ID);
			}
		}
	}
}
function AddOrderProperty($code, $value, $order) {
	if (!strlen($code)) {
		return false;
	}
	if (CModule::IncludeModule('sale')) {
		if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => $code))->Fetch()) {
			return CSaleOrderPropsValue::Add(array(
				'NAME' => $arProp['NAME'],
				'CODE' => $arProp['CODE'],
				'ORDER_PROPS_ID' => $arProp['ID'],
				'ORDER_ID' => $order,
				'VALUE' => $value,
			));
		}
	}
}
?>