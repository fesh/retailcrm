<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";
if (!isset($_REQUEST["name"]) || !isset($_REQUEST["phone"]) || !isset($_REQUEST["id"]) || !isset($_REQUEST["quantity"])) {
	return array("error" => "Переданы не все данные");
}
global $USER;
if (!CModule::IncludeModule('sale')) {
	return;
}
if (!CModule::IncludeModule("catalog")) {
	return;
}

$arUser = array();
$userId = null;
//Если пользователь авторизован - то всё хорошо
if ($USER->IsAuthorized()) {
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	$userId = $USER->GetID();
}
//Иначе создаём нового
else {
//Пытаемся сначала авторизоваться с поступившими данными
	$rsUser = CUser::GetByLogin(htmlspecialcharsbx($_REQUEST["phone"]));
	$arUser = $rsUser->Fetch();
	if (!empty($arUser)) {
		$userId = $arUser["ID"];
		$upUser = new CUser;
		$upUser->Update($userId, array("NAME" => htmlspecialcharsbx($_REQUEST["name"])));
		$arUser["NAME"] = htmlspecialcharsbx($_REQUEST["name"]);
	}
//Если такого пользователя нет, то пытаемся его зарегистрировать
	else {
		$arUser["LOGIN"] = htmlspecialcharsbx($_REQUEST["phone"]);
		$arUser["NAME"] = htmlspecialcharsbx($_REQUEST["name"]);
		$arUser["PERSONAL_PHONE"] = htmlspecialcharsbx($_REQUEST["phone"]);
		$arUser["EMAIL"] = htmlspecialcharsbx($_REQUEST["phone"]) . "@temp.temp";

		$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
		if ($def_group != "") {
			$GROUP_ID = explode(",", $def_group);
			$arPolicy = $USER->GetGroupPolicy($GROUP_ID);
		} else {
			$arPolicy = $USER->GetGroupPolicy(array());
		}
		$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
		if ($password_min_length <= 0) {
			$password_min_length = 6;
		}

		$password_chars = array(
			"abcdefghijklnmopqrstuvwxyz",
			"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
			"0123456789",
		);
		if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y") {
			$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
		}

		$arUser["PASSWORD"] = $arUser["PASSWORD_CONFIRM"] = randString($password_min_length + 2, $password_chars);
		$newUser = new CUser;
		if (!($userId = $newUser->Add($arUser))) {
			echo $arUser->LAST_ERROR;
		}

	}
}

//Создаём заказ
if ($userId !== null) {
	$arOrderFields = array(
		"LID" => "s1",
		"PERSON_TYPE_ID" => 1,
		"PAYED" => "N",
		"CANCELED" => "N",
		"STATUS_ID" => "N",
		"PRICE" => 0,
		"CURRENCY" => "RUB",
		"USER_ID" => $userId,
		"PAY_SYSTEM_ID" => 3,
		"PRICE_DELIVERY" => 0,
		"DELIVERY_ID" => 2,
		"DISCOUNT_VALUE" => 0,
		"TAX_VALUE" => 0.0,
		"USER_DESCRIPTION" => "",
	);
	$ORDER_ID = CSaleOrder::Add($arOrderFields);
//Если заказ был добавлен, то устанавливаем дополнительные свойства заказа
	if ($ORDER_ID) {
		AddOrderProperty("FIO", htmlspecialcharsbx($arUser["NAME"]), $ORDER_ID);
		AddOrderProperty("PHONE", htmlspecialcharsbx($_REQUEST["phone"]), $ORDER_ID);
		AddOrderProperty("orderMethod", "one-click", $ORDER_ID);
	} else {
		$fail = true;
	}

//Привязываем товар к заказу
	if (!Add2BasketByProductID(intval($_REQUEST["id"]), intval($_REQUEST["quantity"]), array('ORDER_ID' => $ORDER_ID), $basket_props)) {
		$fail = true;
	}
	if (!$fail) {
//обновляем параметры заказа на актуальные
		$dbBasketItems = CSaleBasket::GetList(
			array("NAME" => "ASC", "ID" => "ASC"), array(
				"LID" => SITE_ID,
				"ORDER_ID" => $ORDER_ID,
			), false, false, array(
				"ID", "NAME", "QUANTITY", "CURRENCY", "CAN_BUY", "PRICE", "WEIGHT", "DISCOUNT_PRICE",
			)
		);
		$summ = $weight = 0;
		$strOrderList = "";

		$arBasket = array();
		while ($arBasketItems = $dbBasketItems->GetNext()) {
			$strOrderList .= $arBasketItems["NAME"] . " - " . $arBasketItems["QUANTITY"] . " шт на " . SaleFormatCurrency($arBasketItems["PRICE"] * $arBasketItems["QUANTITY"], $arBasketItems["CURRENCY"]);
			$strOrderList .= "\n";
			$summ += roundEx($arBasketItems["PRICE"], SALE_VALUE_PRECISION) * DoubleVal($arBasketItems["QUANTITY"]);
			$weight += doubleval($arBasketItems['WEIGHT']);
			$arBasket[] = $arBasketItems;
		}
		$arOrderForDiscount = array(
			'SITE_ID' => SITE_ID,
			'USER_ID' => $userId,
			'ORDER_PRICE' => $summ,
			'ORDER_WEIGHT' => $weight,
			'BASKET_ITEMS' => $arBasket,
		);
		$arDiscountOptions = array();
		$arDiscountErrors = array();
		CSaleDiscount::DoProcessOrder($arOrderForDiscount, $arDiscountOptions, $arDiscountErrors);

		$arOrderForDiscount['PRICE_BEFORE'] = $summ;
		$arOrderForDiscount['PRICE'] = $arOrderForDiscount['ORDER_PRICE'];
		$arOrderForDiscount['WEIGHT'] = $arOrderForDiscount['ORDER_WEIGHT'];

		if (!CSaleOrder::Update($ORDER_ID, $arOrderForDiscount)) {
			$fail = true;
		}
	}

}

if ($fail) {
	echo (json_encode(array("status" => "error")));
} else {
	echo (json_encode(array("status" => "ok")));
}
?>